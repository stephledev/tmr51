<?php

	require_once('bootstrap.php');

    //Check login
    if(isset($_SESSION['user'])) {
        header('Location: index.php');
    }

    //GET
    if($_SERVER['REQUEST_METHOD'] === 'GET') : ?>

        <?php require_once('partials/header.php'); ?>

            <div class="row">
                <div class="col-xs-12">
                    <?php if(isset($_SESSION['notification_logout'])): ?>
                        <?php unset($_SESSION['notification_logout']); ?>
                        <div class="alert alert-info alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Erfolgreich ausgeloggt.
                        </div>
                    <?php endif; ?>
                    <?php if(isset($_SESSION['notification_error'])): ?>
                        <?php unset($_SESSION['notification_error']); ?>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Login ungültig.
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <h2>Login</h2>
                    <form method="post">
                        <div class="form-group">
                            <label for="username">Benutzer</label>
                            <input type="text" class="form-control" id="username" name="username">
                        </div>
                        <div class="form-group">
                            <label for="password">Passwort</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>

        <?php require_once('partials/footer.php'); ?>


    <?php endif;

    //POST
    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $db->where ('username', $_POST['username']);
        $db->where ('password', $_POST['password']);
        $user = $db->getOne ("user");

        if($user === null) {
            $_SESSION['notification_error'] = true;
            header('Location: login.php?error=1');
        } else {
            $_SESSION['user'] = $user['id'];
            $_SESSION['notification_login'] = true;
            header('Location: index.php');

        }
    }
    ?>