<?php
    require_once(__DIR__.'/bootstrap.php');

    if(!isset($_SESSION['user'])) {
        header('Location: login.php');
    }

    $db->where ("id", $_SESSION['user']);
    $user = $db->getOne('user');

    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $data = [
            'amount' => $_POST['amount'],
            'recipient' => $_POST['recipient'],
            'sender' => $user['account'],
        ];
        $id = $db->insert ('transaction', $data);
        if ($id) {
            header('Location: /');
            $_SESSION['transfer_success'] = true;
        } else {
            $_SESSION['transfer_error'] = true;
        }
    }

    header('Access-Control-Allow-Origin: http://'.$_SERVER['SERVER_NAME'].':9000');
    header('Access-Control-Allow-Credentials: true');

    require_once('partials/header.php'); ?>

    <div class="row">
        <div class="col-xs-12">
            <?php if(isset($_SESSION['transfer_error'])): ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Überweisungs-Daten ungültig.
                </div>
                <?php unset($_SESSION['transfer_error']); ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="nav nav-tabs" data-tabs="tabs">
                <li class="active"><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/">Konto-Übersicht</a></li>
                <li><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/adviser/">Kundenbetreuer</a></li>
                <li class="pull-right"><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/logout.php">Logout</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h2>Überweisung tätigen</h2>
        </div>
    </div>
    <div class="row">
        <form method="post" class="col-xs-6">
            <div class="form-group">
                <label for="recipient">Empfänger</label>
                <input type="text" class="form-control" id="recipient" name="recipient" placeholder="CH00 0000 0000 0000 0000 0">
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="amount">Betrag</label>
                        <input type="text" class="form-control" id="amount" name="amount">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Bestätigen</button></a>
        </form>
    </div>

    <?php require_once('partials/footer.php'); ?>
