<?php
    require_once('bootstrap.php');

    if(!isset($_SESSION['user'])) {
        header('Location: login.php');
    }

    $db->where ("id", $_SESSION['user']);
    $user = $db->getOne('user');

    $db->where ("sender", $user['account']);
    $spendings = $db->get('transaction');
    $spendingsTotal = 0;
    foreach($spendings as $spending) {
        $spendingsTotal += $spending['amount'];
    }

    $db->where ("recipient", $user['account']);
    $earnings = $db->get('transaction');
    $earningsTotal = 0;
    foreach($earnings as $earning) {
        $earningsTotal += $earning['amount'];
    }

    header('Access-Control-Allow-Origin: http://'.$_SERVER['SERVER_NAME'].':9000');
    header('Access-Control-Allow-Credentials: true');

    require_once('partials/header.php'); ?>

        <div class="row">
            <div class="col-xs-12">
                <?php if(isset($_SESSION['notification_login'])): ?>
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Erfolgreich eingeloggt. Willkommen zurück, <?= $user['firstname'] ?> <?= $user['lastname'] ?>!
                    </div>
                    <?php unset($_SESSION['notification_login']); ?>
                <?php endif; ?>
                <?php if(isset($_SESSION['transfer_success'])): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Überweisung erfolgreich getätigt.
                    </div>
                    <?php unset($_SESSION['transfer_success']); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="active"><a href="#">Konto-Übersicht</a></li>
                    <li><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/adviser/">Kundenbetreuer</a></li>
                    <li class="pull-right"><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/logout.php">Logout</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h2>Aktueller Kontostand</h2>
                <h3><?= $user['account'] ?></h3>
                <table class="table table-striped">
                    <thead>
                    <th>Konto-Nr.</th>
                    <th class="text-right">Betrag</th>
                    </thead>
                    <thead>
                    <th>Einnahmen</th>
                    <th class="text-right"><?= $earningsTotal ?></th>
                    </thead>
                    <tbody>
                    <?php foreach($earnings as $earning): ?>
                        <tr>
                            <td><?= $earning['sender'] ?></td>
                            <td class="text-right"><?= $earning['amount'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <thead>
                    <th>Ausgaben</th>
                    <th class="text-right">-<?= $spendingsTotal ?></th>
                    </thead>
                    <tbody>
                    <?php foreach($spendings as $spending): ?>
                        <tr>
                            <td><?= $spending['recipient'] ?></td>
                            <td class="text-right">-<?= $spending['amount'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <thead>
                    <th>Total</th>
                    <th class="text-right"><?= $earningsTotal-$spendingsTotal ?></th>
                    </thead>
                </table>
                <a href="http://<?= $_SERVER['HTTP_HOST'] ?>/add.php"><button class="btn btn-primary">Überweisung erfassen</button></a>
            </div>
        </div>
    </div>

    <?php require_once('partials/footer.php'); ?>
