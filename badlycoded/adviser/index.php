<?php
    require_once(__DIR__.'/../bootstrap.php');

    if(!isset($_SESSION['user'])) {
        header('Location: login.php');
    }

    $db->where ("id", $_SESSION['user']);
    $user = $db->getOne('user');

    $salesmen = $db->get('salesman');

    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $data = [
            'content' => $_POST['comment'],
            'user' => $_SESSION['user'],
            'salesman' => $_POST['salesman'],
            'created' => (new \DateTime('now'))->format('Y-m-d')
        ];
        $id = $db->insert ('comment', $data);
        if ($id) {
            $_SESSION['comment_success'] = true;
        } else {
            $_SESSION['comment_error'] = true;
        }
    }

    header("X-XSS-Protection: 0");

    require_once('../partials/header.php'); ?>

        <div class="row">
            <div class="col-xs-12">
                <?php if(isset($_SESSION['comment_success'])): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Kommentar erfolgreich erstellt.
                    </div>
                    <?php unset($_SESSION['comment_success']); ?>
                <?php endif; ?>
                <?php if(isset($_SESSION['comment_error'])): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?= $db->getLastError(); ?>
                    </div>
                    <?php unset($_SESSION['comment_error']); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/">Konto-Übersicht</a></li>
                    <li class="active"><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/adviser/">Kundenbetreuer</a></li>
                    <li class="pull-right"><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/logout.php">Logout</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h2>Kundenberater Bewertungen</h2>
            </div>
        </div>
        <?php foreach($salesmen as $salesman): ?>
            <div class="row">
                <div class="col-xs-12">
                    <h3><?= $salesman['name']; ?></h3>
                </div>
                <div class="col-xs-6">
                    <img src="http://<?= $_SERVER['HTTP_HOST'].'/assets/'.$salesman['image'] ?>" />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    $db->where ("salesman", $salesman['id']);
                    $comments = $db->get('comment'); ?>
                    <ul class="list-group">
                        <?php foreach($comments as $comment):
                            $db->where ("id", $comment['user']);
                            $user = $db->getOne('user'); ?>
                            <li class="list-group-item">
                                <p>"<?= $comment['content']?>"</p>
                                <p><?= $user['firstname'].' '.$user['lastname'] ?> - <?= (new DateTime($comment['created']))->format('d.m.Y') ?></p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <form method="post">
                        <div class="form-group">
                            <label for="comment">Kommentar</label>
                            <textarea class="form-control" name="comment" id="comment"></textarea>
                        </div>
                        <input type="hidden" name="salesman" value="<?= $salesman['id'] ?>" />
                        <button type="submit" class="btn btn-primary">Absenden</button></a>
                    </form>
                </div>
            </div>
        <?php endforeach; ?>

    <?php require_once('../partials/footer.php'); ?>
