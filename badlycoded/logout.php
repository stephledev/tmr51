<?php

	require_once('bootstrap.php');

    unset($_SESSION['user']);
    $_SESSION['notification_logout'] = true;

    header('Location: login.php');